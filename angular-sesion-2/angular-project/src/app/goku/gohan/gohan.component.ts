import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Technique } from 'src/app/techiques/techniques';

@Component({
  selector: 'app-gohan',
  templateUrl: './gohan.component.html',
  styleUrls: ['./gohan.component.scss']
})
export class GohanComponent implements OnInit {

  @Input() tecnicasGohan: Technique[];
  @Output() emisorTec: EventEmitter<Technique>;

  constructor() {
    this.emisorTec = new EventEmitter<Technique>();
  }

  ngOnInit(): void {
    let tecnicasMedium: Array<Technique>;
    tecnicasMedium = new Array<Technique>();

    for (let i = 0; i < this.tecnicasGohan.length; i++)
    {
      let tec = this.tecnicasGohan[i];
      if (tec.complexity == 'medium' || tec.complexity == 'low') {
        tecnicasMedium.push(tec);
      }      
    };
    this.tecnicasGohan = tecnicasMedium;
  }
  avisarTecAprendida(tecnica: Technique):void {
    this.emisorTec.emit(tecnica);
  }
}
