import { Component, OnInit } from '@angular/core';
import { Technique } from '../techiques/techniques';
import tecnicas from  '../techiques/techniques';

@Component({
  selector: 'app-goku',
  templateUrl: './goku.component.html',
  styleUrls: ['./goku.component.scss']
})
export class GokuComponent implements OnInit {

  title = 'angular-project';
  listadoTec: Technique[];
  tecnicasAprendiasGohan: Technique[];

  constructor() {
    this.listadoTec = tecnicas;
    this.tecnicasAprendiasGohan = new Array<Technique>();
  }
  ngOnInit(): void {
  }
  recibeTec(tecnica: Technique) {
    this.tecnicasAprendiasGohan.push(tecnica);
  }
}
