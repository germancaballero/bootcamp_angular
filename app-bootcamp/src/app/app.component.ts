import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'App Bootcamp en Angular';
  subtitle = 'Componente raiz';
  eliminar: boolean = false;
  mostrar: boolean = true;
}
