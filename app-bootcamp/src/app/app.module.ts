import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { IntroComponent } from './intro/intro.component';
import { IntroHeaderComponent } from './intro-header/intro-header.component';
import { TemplatesComponent } from './templates/templates.component';
import { from } from 'rxjs';
import { DirectivasNativasComponent } from './directivas-nativas/directivas-nativas.component';

@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    IntroHeaderComponent,
    TemplatesComponent,
    DirectivasNativasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
