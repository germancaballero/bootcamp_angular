export class Fruta {
    nombre: string;
    cantidad: number;
    madura: boolean;
    
    constructor(nom: string, cant: number, madura: boolean) {
        this.nombre = nom;
        this.cantidad = cant;   
        this.madura = madura;
    }
    setCantidad(c: number) {
        this.cantidad = c;
    }
}