import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivasNativasComponent } from './directivas-nativas.component';

describe('DirectivasNativasComponent', () => {
  let component: DirectivasNativasComponent;
  let fixture: ComponentFixture<DirectivasNativasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivasNativasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivasNativasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
