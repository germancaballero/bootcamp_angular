import { Component, OnInit } from '@angular/core';
import { Fruta } from '../clases/fruta';

@Component({
  selector: 'app-directivas-nativas',
  templateUrl: './directivas-nativas.component.html',
  styleUrls: ['./directivas-nativas.component.css']
})
export class DirectivasNativasComponent implements OnInit {
// Las directivas también se llaman directivas de componente

  activo: boolean;
  password: string;
  userPasswd: string;
  listaFrutas: Fruta[];
  nombreFruta: string;
  cantidadFruta: number;
  madura: boolean;

  constructor() { }

  ngOnInit(): void {
    this.activo = true;
    this.password = "1234";
    this.listaFrutas = [ 
      new Fruta("mandarina", 0, false),
      new Fruta("mango", 1, false),
      new Fruta("melon", 8, false)
    ];
    // console.log("userPasswd = " + this.userPasswd);
    this.checkPassword();
    this.userPasswd = '';
  }
  addFruta(nomFruta: string, cant: number, madura) {
    console.log("Tipo de cantidad: " + typeof(cant));
    this.listaFrutas.push(new Fruta(nomFruta, 0, madura));
    this.listaFrutas[this.listaFrutas.length-1].setCantidad(cant);
  }
  cambiarEstado(): void {
    this.activo = !this.activo;
  }
  checkPassword(): boolean {
    return this.password === this.userPasswd;
  }
  getHeroes(): Array<string> {
    const listadoHeroes: string[] = ["Superman", "Batman", "Robin"];
    listadoHeroes.push("Ironman");
    return listadoHeroes;
  }
}
