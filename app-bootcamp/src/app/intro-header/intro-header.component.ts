import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-intro-header',
  templateUrl: './intro-header.component.html',
  styleUrls: ['./intro-header.component.css']
})
export class IntroHeaderComponent implements OnInit, OnDestroy {
  titulo = "Introducción";
  
  constructor() { }

  ngOnInit(): void {
  }

  ngOnDestroy() : void {
    console.log("Componente destruido");
  }
}
