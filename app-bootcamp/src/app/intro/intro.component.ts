import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {

  texto: string;
  eliminar = false;

  // El constructor se llama antes de cargarse en el navegador
  constructor() { }

  // Este método se ejecuta una vez que se haya cargado en el navegador el componente
  ngOnInit(): void {
    this.texto = "Valor asignado en ngOnInit()";
  }
  cambiarTexto() : void {
    this.texto = "Texto cambiado por event binding";
  }
  ngOnDestroy() : void {
    console.log("Componente destruido");
  }
  hacerEliminar(): void {
    this.eliminar = true;
  }
}
