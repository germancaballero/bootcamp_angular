import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {
  // La propia clase es el modelo
  fichImagen: string;
  campoTexto: string;
  claseBorde: string;
  clasesArray_1: string[];
  clasesArray_2: Array<string>;
  clasesEnOBJ;

  constructor() { 
    this.fichImagen = "/assets/images/external-content.duckduckgo.com.jpg";
    this.campoTexto = "Valor inicial";
    this.claseBorde = "bordeado";
  }

  ngOnInit(): void {
    this.clasesArray_1 = ['borde-rojo', 'borde-grueso'] ;
    this.clasesArray_2 = ['borde-rojo', 'borde-grueso'] ;
    this.clasesEnOBJ = this.obtenerClasesCSS(false, true);
    console.log("on init");    
  }
  cambiarImagen():void {
    this.fichImagen = "/assets/images/external-content.duckduckgo.com-x.jpg";
  }
  obtenerClasesCSS(rojo: boolean, grueso: boolean): object {
    let objCSS = {'borde-rojo': rojo, 'borde-grueso': grueso};
    return objCSS;
  }
}
