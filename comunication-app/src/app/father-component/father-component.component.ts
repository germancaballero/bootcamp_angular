import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-father-component',
  templateUrl: './father-component.component.html',
  styleUrls: ['./father-component.component.css']
})
export class FatherComponentComponent implements OnInit {
  inputText: string = '';
  sonMessage = '';
  
  constructor() { }

  ngOnInit(): void {
  }
  // con cada tecla apretada se activa esta funcion.
  keyUp(letra: string) {
		this.inputText = letra;
  }
   // Recibe el mensaje del hijo	
   setMessage(message: string): void {
    this.sonMessage = message;
  }
}
