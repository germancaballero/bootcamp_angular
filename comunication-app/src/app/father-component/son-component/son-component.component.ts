import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-son-component',
  templateUrl: './son-component.component.html',
  styleUrls: ['./son-component.component.css']
})
export class SonComponentComponent implements OnInit {
  @Input() 
  inputTextSon: string;
  @Input() 
  inputTextLower: string = '';

  @Output() emitMessage;
  
  message = '';

  constructor() { 
    // El tipo de dato que envía es un string
    this.emitMessage = new EventEmitter<string>();
  }

  ngOnInit(): void {
  }
  // Método que va a hacer la emisión
  sendMessage(): void {
    this.emitMessage.emit("Dato enviado: " + this.message);
  }
}
