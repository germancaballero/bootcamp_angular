import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { Student } from './student';
import { StudentListComponent } from './student-list/student-list.component';

// Variable con la lista de rutas de nuestra aplicación
const routes: Routes = [
    // aquí van nuestras rutas
    { path: '', redirectTo: 'formulario', pathMatch: 'full'},
    { path: 'formulario', component: SignupFormComponent},
    { path: 'estudiantes', component: StudentListComponent},
    { path: 'estudiantes/:indexStudent', component: StudentListComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}