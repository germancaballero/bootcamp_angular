import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators  } from '@angular/forms';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  user: FormGroup;
  // private fb: FormBuilder;

  // Angular detecta que al construir el componente, necesita de un FormBuilder
  // por lo que lo crea, y nos lo pasa como parámetro
  // a esto se le llama "inyección de dependencias"
  constructor(private fb: FormBuilder) { 
    // this.fb = fb;
  }

  ngOnInit(): void {    
    
    this.user = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      password: new FormControl('', Validators.required),
      passwordRepeat: new FormControl('', Validators.required)
    }, { validators: this.passwordMatchValidator});
  }
  passwordMatchValidator(g: FormGroup): any {
    return g.get('password').value === g.get("passwordRepeat").value
      ? null : {'mismatch': true};
  }
  onSubmit() : void {
    alert("Enviando formulario");
  }
}
