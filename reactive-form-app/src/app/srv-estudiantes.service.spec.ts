import { TestBed } from '@angular/core/testing';

import { SrvEstudiantesService } from './srv-estudiantes.service';

describe('SrvEstudiantesService', () => {
  let service: SrvEstudiantesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SrvEstudiantesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
