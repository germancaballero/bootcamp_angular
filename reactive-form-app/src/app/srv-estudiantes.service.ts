import { Injectable } from '@angular/core';
import { Student} from './student';

/* Es donde debemos centralizar la lógica de la aplicación y los datos */
@Injectable({
  providedIn: 'root'
})
export class SrvEstudiantesService {

  private estudiantes: Array<Student>;

  constructor() {
    this.estudiantes = new Array<Student>();
    this.estudiantes.push({email: "alumn_0@email", edad: 20});
    this.estudiantes.push({email: "alumn_1@email", edad: 21});
    this.estudiantes.push({email: "alumn_2@email", edad: 22});
 }
 addStudent(nuevoStudent: Student) {
   this.estudiantes.push(nuevoStudent);
 }
 getListaEstudiantes() {
   return this.estudiantes;
 }
}
