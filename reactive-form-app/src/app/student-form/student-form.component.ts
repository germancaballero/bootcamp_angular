import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { Student} from '../student';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  estudiante: Student;
  estudianteFG: FormGroup;
  @Output() emisorEstudiante: EventEmitter<Student>;

  constructor(private fb: FormBuilder) { 
    this.estudiante = new Student();
    this.estudianteFG = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(4), Validators.email]],
      edad: ['', [Validators.required, Validators.min(1), Validators.max(199)]]
    });
    this.emisorEstudiante = new EventEmitter<Student>();
  }
  ngOnInit(): void {
  }
  alRegistrar(): void {
    this.emisorEstudiante.emit(this.estudiante);
    this.estudiante = new Student();
  }
}
