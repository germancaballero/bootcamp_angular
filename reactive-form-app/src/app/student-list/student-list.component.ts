import { Component, OnInit } from '@angular/core';
import { Student} from '../student';
import { ActivatedRoute } from '@angular/router';
import { SrvEstudiantesService } from '../srv-estudiantes.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  index: number = -1;


  constructor(
        private route: ActivatedRoute,
        public srvEstu: SrvEstudiantesService ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const comprobacion = parseInt(params.get('index'), 10);
      if (comprobacion < this.srvEstu.getListaEstudiantes().length){
        this.index = comprobacion;
      }
      //console.log(this.index);
    });
  }
  recibirEstudiante(nuevoEstudiante: Student) {
    this.srvEstu.addStudent(nuevoEstudiante);
  }
  
}
