var lenguaje = "TypeScript";
console.log(lenguaje + " es JavaScript");
var tipado = "Fuerte";
console.log("Pero con tipado " + tipado);
tipado = 100;
// También podemos indicar aposta de qué tipo es una variable
var edad;
edad = 50;
edad = true;
