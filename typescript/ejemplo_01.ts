var lenguaje = "TypeScript";
console.log(lenguaje + " es JavaScript");

let tipado = "Fuerte";
console.log("Pero con tipado " + tipado);

tipado = 100;
// También podemos indicar aposta de qué tipo es una variable
let edad: number;
edad = 50;
edad = true;
 