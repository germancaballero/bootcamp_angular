// TypeScript además del tipado fuerte (opcional) tiene P.O.O. como lenguajes robustos tipo Java
// Se basa en clases e instancias de éstas (objetos)
// 1 - Encapsulación: private, protected, public: 
// 2 - Herencia: Una clase puede heredar los métodos y propiedades de otra clase
// 3 - Polimorfismo: Un objeto de un tipo (una clase), puede tener su forma o la de sus padres, pero no al revés
// Además tiene otras carácteristicas como interfaces o uniones
var Usuario = /** @class */ (function () {
    function Usuario() {
    }
    Usuario.prototype.enTexto = function () {
        return "Usuario: " + this.nombre + ", " + this.edad; // template string de ES6
    };
    return Usuario;
}());
var yoMismo = new Usuario(); // TS siempre genera un constructor por defecto
yoMismo.nombre = "Germán";
yoMismo.edad = 37;
console.log(yoMismo.enTexto());
var NumCuenta = /** @class */ (function () {
    // El constructor recibe unos parámetros que usualmente corresponden con las variables de la clase
    function NumCuenta(iban, nom, cs, infoExtra) {
        this.iban = iban;
        this.nombre = nom;
        this.codSeg = cs;
        this.infoExtra = infoExtra;
    }
    NumCuenta.prototype.mostrar = function () {
        console.log("Iban: " + this.iban);
        console.log(" - Nombre: " + this.nombre);
        console.log(" - Cod. Seg.: " + this.codSeg);
        // return "dfdf";  // Error, no se puede devolver nada con void
    };
    NumCuenta.prototype.hayInfoExtra = function () {
        return this.infoExtra !== null && typeof this.infoExtra !== "undefined";
    };
    NumCuenta.prototype.enTexto = function () {
        return "Cuenta " + this.iban + ", " + this.nombre;
    };
    return NumCuenta;
}());
var miCuenta = new NumCuenta("3434 1112", "Fulanito", 2, true);
var otraCuenta = new NumCuenta("111 2222", "Mengano", "07", [1, 2, 3, 4]);
var cuentaEnSuiza = new NumCuenta("5555 7777", "Juanqui", 99, null);
console.log(miCuenta.enTexto());
// miCuenta.mostrar();
otraCuenta.mostrar();
cuentaEnSuiza.mostrar();
if (miCuenta.hayInfoExtra())
    console.log(miCuenta.infoExtra);
if (otraCuenta.hayInfoExtra())
    console.log(otraCuenta.infoExtra);
if (cuentaEnSuiza.hayInfoExtra())
    console.log(cuentaEnSuiza.infoExtra);
