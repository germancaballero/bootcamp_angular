// TypeScript además del tipado fuerte (opcional) tiene P.O.O. como lenguajes robustos tipo Java
// Se basa en clases e instancias de éstas (objetos)
// 1 - Encapsulación: private, protected, public: 
// 2 - Herencia: Una clase puede heredar los métodos y propiedades de otra clase
// 3 - Polimorfismo: Un objeto de un tipo (una clase), puede tener su forma o la de sus padres, pero no al revés
// Además tiene otras carácteristicas como interfaces o uniones

// Una interfaz en programación es la manera que deben comunicarse LOS OBJETOS
// Para ello, como en TS hablamos de clase, en esencia una interfaz indica qué métodos debe tener una clase.
interface ConvertibleEnTexto {  // 
    enTexto(): string;  // Pero no como deben estar implementados, eso lo hace cada clase a su manera
}

class Usuario implements ConvertibleEnTexto {
    nombre: string;
    edad: number;    

    enTexto(): string {
        return `Usuario: ${this.nombre}, ${this.edad}`; // template string de ES6
    }
}
let yoMismo: Usuario = new Usuario();   // TS siempre genera un constructor por defecto
yoMismo.nombre = "Germán";
yoMismo.edad = 37;
console.log(yoMismo.enTexto());


class NumCuenta implements ConvertibleEnTexto {
    iban: string;
    nombre: string;
    codSeg: number | string;    // union, o bien un tipo o bien el otro. Igual any, mejor no usar
    infoExtra: any; // any es peligroso, intentar no usar

    // El constructor recibe unos parámetros que usualmente corresponden con las variables de la clase
    constructor (iban: string, nom: string, cs: number | string, infoExtra: any) {

        this.iban = iban;
        this.nombre = nom;
        this.codSeg = cs;
        this.infoExtra = infoExtra;
    }
    mostrar(): void {   // Después de los ":", indicamos qué tipo de dato devuelve la función, en este caso NADA
        console.log("Iban: " + this.iban);
        console.log(" - Nombre: " + this.nombre);
        console.log(" - Cod. Seg.: " + this.codSeg);
        // return "dfdf";  // Error, no se puede devolver nada con void
    }
    hayInfoExtra(): boolean {
        return this.infoExtra !== null && typeof this.infoExtra !== "undefined";
    }
    enTexto() : string {
        return `Cuenta ${this.iban}, ${this.nombre}`;
    }
}

let miCuenta : NumCuenta = new NumCuenta("3434 1112", "Fulanito", 2, true);
let otraCuenta : NumCuenta = new NumCuenta("111 2222", "Mengano", "07", [1,2,3,4]);
let cuentaEnSuiza: NumCuenta = new NumCuenta("5555 7777", "Juanqui", 99, null);
console.log(miCuenta.enTexto());

 // miCuenta.mostrar();
otraCuenta.mostrar();
cuentaEnSuiza.mostrar();

if (miCuenta.hayInfoExtra())  console.log(miCuenta.infoExtra);
if (otraCuenta.hayInfoExtra())  console.log(otraCuenta.infoExtra);
if (cuentaEnSuiza.hayInfoExtra())  console.log(cuentaEnSuiza.infoExtra);
